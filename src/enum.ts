export enum Priority{
    one = "1 - Низкий", 
    two = "2 - Средний",
    three = "3 - Высокий", 
    four = "4 - Критичный"
}

export enum TaskStatus {
    undone = "1 - не выполнено",
    done = "2 - выполнено",
    holdOver = "4 - отложено",
    fail = "3 - провалено"
  };
import {Priority, TaskStatus} from './enum'

export interface Task{
    userName:string;
    nameTask:string;
    dateDeadline:Date|null;
    priority:Priority;
    category:string;
    description:string;
    status:TaskStatus;
}

export interface User{
    firstName:string;
    lastName:string;
    userName:string;
    password:string;
    userTasks: Task[];
    categories: string[];
    soonDeadline:number;
}


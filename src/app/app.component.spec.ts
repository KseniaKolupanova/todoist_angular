import { CommonModule } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { SharedModule } from './modules/shared/shared.module';
import { AutorizationService } from './services/autorization.service';

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        MatDialogModule,
        CommonModule,
        FormsModule, 
        ReactiveFormsModule,
        SharedModule 
      ],
      providers: [
        {
          provide: MatDialog,
          useValue: {}
        },
        { provide: Router, useValue: {} },
        AutorizationService
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it('Вернёт пустое поле логина пользователя',()=>{
    const fixture = TestBed.createComponent(AppComponent);
    const component = fixture.componentInstance;
    component.logOut();
    expect(component.userInfo$.value).toHaveSize(0);
  });

  
});

import { HttpEvent, HttpHandler, HttpInterceptor, HttpParams, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { User, Task } from 'src/interface';


@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor{

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
   
    const { url, method, headers, body } = req;
    
    
    switch (true) {
      case url.endsWith('/user') && method === 'GET':
        return this.loadCurrentUser();
      case url.match(/\/login\/\w+$/) && method === 'POST':
        return this.loginUser(body);
      case url.endsWith('/logout') && method === 'GET':
        return this.logOutUser();
      case url.endsWith('/check-username') && method === 'GET':
        return this.checkExistUser(req.params);
      case url.endsWith('/create-user') && method === 'POST':
        return this.createUser(body);
      case url.match(/\/change-deadline\/\w+$/) && method === 'POST':
        return this.changeDeadline(body);
      case url.match(/\/add-task\/\w+$/) && method === 'POST':
        return this.addTask(body);
      case url.match(/\/edit-task\/\w+$/) && method === 'POST':
        return this.changeTask(body.index, body.editTask);
      case url.match(/\/edit-category\/\w+$/) && method === 'POST':
        return this.changeCategory(body.userName, body.formCategory);
      case url.match(/\/delete-task\/\w+$/) && method === 'POST':
        return this.deleteTask(body.index, body.userName);
      case url.match(/\/delete-category\/\w+$/) && method === 'POST':
        return this.deleteCategory(body.deleted, body.userName);
    }
    
    return next.handle(req);
  }

  loadCurrentUser() : Observable<HttpEvent<any>>{
    
    const currentUser =  localStorage.getItem("currentUser");
    let user :User = {     
      userName: "",  
      password: "",
      lastName: "",
      firstName: "",
      categories: [],
      userTasks:  [],
      soonDeadline:1     
    };

    if (currentUser){
        const currentUserInfo = localStorage.getItem(currentUser);
        if(currentUserInfo != null){
            user = JSON.parse(currentUserInfo);
        }
    }
    return this.sendResponse(user, 200);
    //return throwError(() => new Error("Не могу загрузить информацию о юзере"))
  }

  loginUser (body : {userName: string, password:string}):Observable<HttpEvent<any>>{
      const { userName, password } = body;
      const currentUserInfo =  localStorage.getItem(userName);
      if (currentUserInfo){
          const user = JSON.parse(currentUserInfo);
          if(user.password === password){
              localStorage.setItem("currentUser", userName);
              return this.sendResponse("ok", 200);
          }
          else{
              return this.sendResponse("Пароль не верный", 404);
          }
      } 
      else{
          return this.sendResponse("Не найден пользователь", 404);
      } 
  }

  logOutUser():Observable<HttpEvent<any>>{
    const currentUser = "";
    localStorage.setItem("currentUser", currentUser);
    return this.sendResponse("ok", 200);
  }

  createUser(body: FormGroup) : Observable<HttpEvent<any>>{
        
    const formValue = body.getRawValue();
    const currentUser = formValue.login;
    localStorage.setItem("currentUser", currentUser);
    
    const user = {     
        userName: currentUser,  
        password: formValue.password,
        lastName: formValue.lastname,
        firstName: formValue.firstname,
        categories: [],
        userTasks:  [],
        soonDeadline:1    
      };
    
    localStorage.setItem(currentUser, JSON.stringify(user));
    return this.sendResponse("ok", 200);
  }

  changeDeadline(body : {deadLine : number, userName : string}) : Observable<HttpEvent<any>>{
    const currentUserInfo =  localStorage.getItem(body.userName);
    if(currentUserInfo!=null)
    {
        const user = JSON.parse(currentUserInfo);
        user.soonDeadline = body.deadLine;
        localStorage.setItem(body.userName, JSON.stringify(user));
        
    }
    return this.sendResponse("ok", 200);
  }

  checkExistUser(userName : HttpParams) : Observable<HttpEvent<any>>{
    let currentUserInfo = null;
    const name = userName.get('userName');
    if(name!==null){
      currentUserInfo = localStorage.getItem(name);
    }
    const existFlag : boolean = currentUserInfo !== null;
    return this.sendResponse(existFlag, 200);
  }

  addTask(newTask:Task) : Observable<HttpEvent<any>>{

    const currentUserInfo =  localStorage.getItem(newTask.userName);
    if(currentUserInfo)
    {
        const user = JSON.parse(currentUserInfo);
        user.userTasks.push(newTask);
        if(user.categories.indexOf(newTask.category) === -1 && newTask.category !== null)
        {
            user.categories.push(newTask.category);
        }
        localStorage.setItem(newTask.userName, JSON.stringify(user));
    }
    return this.sendResponse("ok", 200);
  }

  changeTask(index:number, editTask:Task):Observable<HttpEvent<any>>{
    const currentUserInfo =  localStorage.getItem(editTask.userName);
    if(currentUserInfo)
    {
        const user = JSON.parse(currentUserInfo);
        user.userTasks[index] = editTask;
        //this.tasksCurrentUser$.next(this.user.userTasks);
        if(user.categories.indexOf(editTask.category) === -1 && editTask.category != null)
        {
            user.categories.push(editTask.category);
        }
        localStorage.setItem(editTask.userName, JSON.stringify(user));                
    }
    return this.sendResponse("ok", 200);
  }

  changeCategory(userName:string, formCategory:FormArray):Observable<HttpEvent<any>>{
    const formValue = formCategory.getRawValue(); 
    const newCategory = formValue.map((val)=>val.name);
    const currentUserInfo =  localStorage.getItem(userName);
    if(currentUserInfo && newCategory != [])
    {
        const user = JSON.parse(currentUserInfo);
        user.categories = newCategory;
        localStorage.setItem(userName, JSON.stringify(user));
        
    }
    
    return this.sendResponse("ok", 200);
  }

  deleteTask(index:number, userName:string):Observable<HttpEvent<any>>{
    const currentUserInfo =  localStorage.getItem(userName);
    if(currentUserInfo)
    {
        const user = JSON.parse(currentUserInfo);
        if(index === -1){
            user.userTasks = [];
        }
        else{
            user.userTasks.splice(index, 1);
        }
        
        //this.tasksCurrentUser$.next(this.user.userTasks);
        localStorage.setItem(userName, JSON.stringify(user));                
    }
    
    return this.sendResponse("ok", 200);
  }

  deleteCategory(deleted:string, userName:string):Observable<HttpEvent<any>>{
    const currentUserInfo =  localStorage.getItem(userName);
    if(currentUserInfo)
    {
        const user = JSON.parse(currentUserInfo);
        const index = user.categories.indexOf(deleted);
        if (index !== -1) {
            user.categories.splice(index, 1);
        }
        localStorage.setItem(userName, JSON.stringify(user));
    }
    
    return this.sendResponse("ok", 200);
  }

  sendResponse(body:User|string|boolean, numberError : number) : Observable<HttpEvent<any>>{
    return of(new HttpResponse({ status: numberError, body }))
  }

}

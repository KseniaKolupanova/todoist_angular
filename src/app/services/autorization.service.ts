import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { BehaviorSubject, Observable } from 'rxjs';
import { Task, User } from '../../interface';

@Injectable({
    providedIn: 'root'
  })
export class AutorizationService{

    public currentUser$ : BehaviorSubject<User> = new BehaviorSubject({} as User);
    

    constructor(private httpClient:HttpClient){}
    
    getUser() : Observable <User> {
        return this.httpClient.get<User>('/user');
    }

    loginUser(loginInfo : {userName: string, password:string}) : Observable <string> {
        return this.httpClient.post<string>('/login/'+loginInfo.userName, loginInfo);
    }

    logOutUser() : Observable <Object>{
        return this.httpClient.get('/logout');
    }

    createUser(formGroup: FormGroup) : Observable <string>{
        return this.httpClient.post<string>('/create-user',formGroup)
    } 
    
    changeDeadline(value : {deadLine : number, userName : string}) : Observable <string>{
        return this.httpClient.post<string>('/change-deadline/'+value.userName,value);
    } 
    
    checkExistUser(userName:string) : Observable <boolean>{
        return this.httpClient.get<boolean>('/check-username', {params: {userName:userName}});
    }

    addTask(newTask:Task) : Observable <string>{
        return this.httpClient.post<string>('/add-task' + '/'+newTask.userName,newTask);
    }

    deleteTask(index:number, userName:string) : Observable <string>{
        return this.httpClient.post<string>('/delete-task' + '/'+userName,{index:index, userName:userName});
    }

    changeTask(index:number, editTask:Task) : Observable <string>{
        return this.httpClient.post<string>('/edit-task' + '/'+editTask.userName,{index:index, editTask:editTask});
    }

    changeCategory(formCategory:FormArray, userName: string) : Observable <string>{
        return this.httpClient.post<string>('/edit-category' + '/'+userName,{userName:userName, formCategory:formCategory});
    }

    deleteCategory(deleted:string, userName: string): Observable <string>{
        return this.httpClient.post<string>('/delete-category' + '/'+userName,{userName:userName, deleted:deleted});
    }



    

    

    // checkExistCategory(find:string):boolean{
    //     const currentUser = this.getCurrentUser();
    //     if(currentUser){
    //         const currentUserInfo =  localStorage.getItem(currentUser);
    //         if(currentUserInfo)
    //         {
    //             this.user = JSON.parse(currentUserInfo);
    //             const index = this.user.categories.indexOf(find);
    //             if (index !== -1) {
    //                 return true;
    //             }
    //             else{
    //                 return false;
    //             }
    //         }
    //         return false;
    //     }
    //     return false;
    // }

    

    

    
}
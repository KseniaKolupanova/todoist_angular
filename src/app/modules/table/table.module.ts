import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmComponent } from 'src/app/components/confirm/confirm.component';
import { TableComponent } from './table.component';
import { AddDialogComponent } from 'src/app/components/add-dialog/add-dialog.component';
import { EditDialogComponent } from 'src/app/components/edit-dialog/edit-dialog.component';
import { SharedModule } from 'src/app/modules/shared/shared.module';



@NgModule({
  declarations: [
    TableComponent,
    AddDialogComponent,
    EditDialogComponent,
    ConfirmComponent
  ],
  exports:[
    TableComponent,
    AddDialogComponent,
    EditDialogComponent,
    ConfirmComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class TableModule { }

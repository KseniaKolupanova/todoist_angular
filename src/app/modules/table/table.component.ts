import { Task, User } from 'src/interface';
import { AutorizationService } from '../../services/autorization.service';

import { animate, state, style, transition, trigger } from '@angular/animations';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { BehaviorSubject, Subject, Subscription, takeUntil } from 'rxjs';
import { TaskStatus } from 'src/enum';
import { AddDialogComponent } from '../../components/add-dialog/add-dialog.component';
import { ConfirmComponent } from '../../components/confirm/confirm.component';
import { EditDialogComponent } from '../../components/edit-dialog/edit-dialog.component';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class TableComponent implements AfterViewInit, OnInit, OnDestroy {
  displayedColumns: string[] = ['nameTask', 'dateDeadline', 'priority', 'category', 'status', 'edit', 'delete'];
  columnsToDisplayWithExpand = [...this.displayedColumns, 'expand'];
  userTask$ : BehaviorSubject<Task[]> = new BehaviorSubject({} as Task[]);
  user$ : BehaviorSubject<User> = new BehaviorSubject({} as User);
  destroy$: Subject<boolean> = new Subject<boolean>();
  dataSource = new MatTableDataSource<Task>([]);
  expandedElement: Task | null = null;
  
  @ViewChild(MatTable) matTable : MatTable<Task>|undefined;
  @ViewChild(MatSort) matSort: MatSort|undefined;
  

  constructor(private autorizationService:AutorizationService, private _liveAnnouncer: LiveAnnouncer, public dialog: MatDialog){    
  }

  checkDeadline(status:TaskStatus, date:Date) : boolean{
    if(status===TaskStatus.undone){
      const dateNow = new Date();
      const dateDeadline = new Date(date);
      const diff = dateDeadline.getTime() - dateNow.getTime();
      const diffDays = Math.ceil(diff / (1000 * 3600 * 24));
      return diffDays<=this.user$.value.soonDeadline;
    }
    return false;
  }

  updateTable(value:User) : void{
    this.user$.next(value)
    this.userTask$?.next(value.userTasks);
    this.dataSource.data = <Task[]>this.userTask$?.value;
    this.dataSource.sort = <MatSort>this.matSort;
  }


  ngOnInit(): void {
    
    this.autorizationService.currentUser$
    .pipe(takeUntil(this.destroy$))
    .subscribe(values => {this.updateTable.call(this,values)});
    this.user$.next(this.autorizationService.currentUser$.value);
    this.userTask$?.next(this.autorizationService.currentUser$.value.userTasks);
    
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  ngAfterViewInit() : void {
    this.dataSource.sort = <MatSort>this.matSort;
  }

  addDialog() : void{
    const dialogRef = this.dialog.open(AddDialogComponent, {data : {name : this.user$.value.userName, category: this.user$.value.categories}});
    dialogRef.afterClosed().subscribe(value => this.closeAddDialog.call(this,value));
  }

  closeAddDialog(value:Task) : void{
    
    if(value){
      this.autorizationService.addTask(value)
      .pipe(takeUntil(this.destroy$))
      .subscribe();
      this.autorizationService.getUser()
      .pipe(takeUntil(this.destroy$))
      .subscribe(val=>
        this.autorizationService.currentUser$.next(val)
      );
      this.dataSource.sort = <MatSort>this.matSort;
    }
    
  }

  clearData() : void{
    this.dataSource.data = [];
    this.userTask$?.next([]);
    this.autorizationService.deleteTask(-1, this.user$.value.userName)
    .pipe(takeUntil(this.destroy$))
    .subscribe();
    this.autorizationService.getUser()
    .pipe(takeUntil(this.destroy$))
    .subscribe(val=>
      this.autorizationService.currentUser$.next(val)
    );
  }

  deleteTask(i:number) : void{
    
    const dialogRef = this.dialog.open(ConfirmComponent);
    dialogRef.afterClosed().subscribe(value => this.deleteTaskConfirm.call(this,value, i));
    
  }

  deleteTaskConfirm(result:boolean|undefined|string, index:number) : void{
    
    if(result){
      this.autorizationService.deleteTask(index, this.user$.value.userName)
      .pipe(takeUntil(this.destroy$))
      .subscribe();
      this.autorizationService.getUser()
      .pipe(takeUntil(this.destroy$))
      .subscribe(val=>
        this.autorizationService.currentUser$.next(val)
      );
      this.dataSource.sort = <MatSort>this.matSort;
    }
  }

  editTask(index:number) : void{
    
    const dialogRef = this.dialog.open(EditDialogComponent, 
      {data : {name: this.user$.value.userName, 
        task : this.userTask$?.value[index], 
        category: this.user$.value.categories}
      });
    dialogRef.afterClosed().subscribe(value => this.closeEditDialog.call(this,value, index));
  }

  returnStatus(index:number) : void{
    
    this.userTask$!.value[index].status = TaskStatus.undone;
    this.userTask$!.value[index].dateDeadline = this.addDays(this.user$.value.soonDeadline+1);
    //this.autorizationService.changeTask(index, this.userTask$!.value[index]);
    this.autorizationService.changeTask(index, this.userTask$!.value[index])
      .pipe(takeUntil(this.destroy$))
      .subscribe();
    this.autorizationService.getUser()
    .pipe(takeUntil(this.destroy$))
    .subscribe(val=>
      this.autorizationService.currentUser$.next(val)
    );
  }

  addDays(days:number) : Date {
    
    const result = new Date();
    result.setDate(result.getDate() + days);
    return result;
  }

  holdOverStatus(index:number) : void{
    
    this.userTask$!.value[index].status = TaskStatus.holdOver;
    this.userTask$!.value[index].dateDeadline = null;
    this.autorizationService.changeTask(index, this.userTask$!.value[index])
      .pipe(takeUntil(this.destroy$))
      .subscribe();
    this.autorizationService.getUser()
    .pipe(takeUntil(this.destroy$))
    .subscribe(val=>
      this.autorizationService.currentUser$.next(val)
    );
  }

  failStatus(index:number) : void{
    
    this.userTask$!.value[index].status = TaskStatus.fail;
    this.autorizationService.changeTask(index, this.userTask$!.value[index])
      .pipe(takeUntil(this.destroy$))
      .subscribe();
    this.autorizationService.getUser()
    .pipe(takeUntil(this.destroy$))
    .subscribe(val=>
      this.autorizationService.currentUser$.next(val)
    );
  }

  doneStatus(index:number) : void{
    
    this.userTask$!.value[index].status = TaskStatus.done;
    this.autorizationService.changeTask(index, this.userTask$!.value[index])
      .pipe(takeUntil(this.destroy$))
      .subscribe();
    this.autorizationService.getUser()
    .pipe(takeUntil(this.destroy$))
    .subscribe(val=>
      this.autorizationService.currentUser$.next(val)
    );
  }

  closeEditDialog(value:Task, index:number) : void{

    if(value){
      this.autorizationService.changeTask(index, value)
      .pipe(takeUntil(this.destroy$))
      .subscribe();
      this.autorizationService.getUser()
      .pipe(takeUntil(this.destroy$))
      .subscribe(val=>
        this.autorizationService.currentUser$.next(val)
      );
      this.dataSource.sort = <MatSort>this.matSort;
    }
    
  }


  announceSortChange(sortState: Sort) : void {
    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }
}



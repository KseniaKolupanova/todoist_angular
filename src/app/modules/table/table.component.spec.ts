import { LiveAnnouncer } from '@angular/cdk/a11y';
import { CommonModule } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { convertPropertyBinding } from '@angular/compiler/src/compiler_util/expression_converter';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RouterTestingModule } from '@angular/router/testing';
import { AutorizationService } from 'src/app/services/autorization.service';
import { Priority, TaskStatus } from 'src/enum';
import { User } from 'src/interface';
import { SharedModule } from '../shared/shared.module';

import { TableComponent } from './table.component';

const USER:User = {
  userName:"ksenia",
  firstName:"Ксения",
  lastName:"Колупанова",
  password:"123456",
  soonDeadline:5,
  categories:["дом", "работа", "личное"],
  userTasks: [{
    userName: "ksenia",
    nameTask : "Работать", 
    dateDeadline: new Date(),
    priority:Priority.one,
    category:"работа",
    description:"",
    status:TaskStatus.undone}]
};

describe('TableComponent', () => {
  let component: TableComponent;
  let fixture: ComponentFixture<TableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        MatDialogModule,
        CommonModule,
        FormsModule, 
        ReactiveFormsModule,
        SharedModule 
      ],
      providers: [
        {
          provide: MatDialog,
          useValue: {}
        },
        { provide: LiveAnnouncer, useValue: {} },
        AutorizationService
      ],
      declarations: [ TableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Вернёт false, если дата меньше даты deadline больше, чем на пять дней', ()=>{
    component.user$.value.soonDeadline = 5;
    const result : boolean = component.checkDeadline(TaskStatus.undone, new Date((new Date()).getFullYear()+1, 11, 31));
    expect(result).toBe(false);
  });

  it('Обновит табличные данные', ()=>{
    
    component.updateTable(USER);
    expect(component.dataSource).not.toHaveSize(0);
  });

  it('Очистит табличные данные', ()=>{
    component.updateTable(USER);
    component.clearData();
    expect(component.dataSource.data).toHaveSize(0);
  });


  it('Вернёт статус задачи undone', ()=>{
    component.updateTable(USER);
    component.returnStatus(0);
    expect(component.user$.value.userTasks[0].status).toBe(TaskStatus.undone);
  });

  it('Вернёт статус задачи holdOver', ()=>{
    
    component.updateTable(USER);
    component.holdOverStatus(0);
    expect(component.user$.value.userTasks[0].status).toBe(TaskStatus.holdOver);
  });

  it('Вернёт статус задачи fail', ()=>{
    
    component.updateTable(USER);
    component.failStatus(0);
    expect(component.user$.value.userTasks[0].status).toBe(TaskStatus.fail);
  });

  it('Вернёт статус задачи done', ()=>{
    component.updateTable(USER);
    component.doneStatus(0);
    expect(component.user$.value.userTasks[0].status).toBe(TaskStatus.done);
  });
});

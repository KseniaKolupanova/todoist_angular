import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistrationComponent } from './registration.component';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    RegistrationComponent
    
  ],
  exports:[
    RegistrationComponent
    
  ], 
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{path:'', component: RegistrationComponent}])
  ]
})
export class RegistrationModule { }

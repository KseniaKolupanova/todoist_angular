import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { AutorizationService } from '../../services/autorization.service';
import { CheckPasswordService } from '../../services/check-password.service';


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'], 
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegistrationComponent implements OnDestroy{

  registerForm:FormGroup;
  isInfoDialogVisible: boolean = false;
  destroy$: Subject<boolean> = new Subject<boolean>();
  
  constructor(private autorizationService: AutorizationService, private router:Router) 
  { 
    this.registerForm = new FormGroup({
      password: new FormControl(null, [Validators.required,Validators.minLength(6)]),
      login: new FormControl(null, [Validators.required]),
      firstname: new FormControl(null, [Validators.required]),
      lastname: new FormControl(null, [Validators.required]),
      pswCheck: new FormControl(null, [Validators.required])
    },CheckPasswordService.passwordMatch("password", "pswCheck"));
    
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
  
  OnRegistration():void{
    this.autorizationService.createUser(this.registerForm)
    .subscribe(
      val=>{
        if(val==="ok"){
          this.autorizationService.getUser()
              .pipe(
                takeUntil(this.destroy$)
              )
              .subscribe(
                val=>this.autorizationService.currentUser$.next(val)
              );
          this.router.navigate(['/tasks']);
        }
      }
    );
    
  }

  checkLogin():boolean{  
    const userName = this.registerForm.get('login')?.value;
    let existFlag:boolean = false;
    this.autorizationService.checkExistUser(userName)
    .pipe(takeUntil(this.destroy$))
    .subscribe(val=> existFlag = val);
    
    return existFlag;
  }
}

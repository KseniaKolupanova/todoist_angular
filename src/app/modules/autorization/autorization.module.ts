import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutorizationComponent } from './autorization.component';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { RouterModule } from '@angular/router';
//import { InfoDialogComponent } from 'src/app/info-dialog/info-dialog.component';



@NgModule({
  declarations: [
    AutorizationComponent
    
  ],
  exports:[
    AutorizationComponent
    
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{path:'', component: AutorizationComponent}])
  ]
})
export class AutorizationModule { }

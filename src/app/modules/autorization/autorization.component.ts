import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { AutorizationService } from 'src/app/services/autorization.service';
import { InfoDialogComponent } from '../../components/info-dialog/info-dialog.component';

@Component({
  selector: 'app-autorization',
  templateUrl: './autorization.component.html',
  styleUrls: ['./autorization.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class AutorizationComponent implements OnDestroy{
 
	autorizeForm : FormGroup;
  destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(private autorizationService: AutorizationService, private router:Router,  public dialog: MatDialog) {
    this.autorizeForm = new FormGroup({
      password: new FormControl(null, [Validators.required,Validators.minLength(6)]),
      login: new FormControl(null, [Validators.required])
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  OnLogin():void{

    this.autorizeForm.getError('login');
    let message:string;
    this.autorizationService.loginUser({userName: this.autorizeForm.get('login')?.value, password: this.autorizeForm.get('password')?.value})
    .subscribe(val => {
      message = val;
      if(message != "ok"){
        this.dialog.open(InfoDialogComponent, {data : {header:"Ошибка", description:message}});
      }
      else{
        this.router.navigate(['/tasks']);
        this.autorizationService.getUser()
        .pipe(
          takeUntil(this.destroy$)
        )
        .subscribe(
          val=>this.autorizationService.currentUser$.next(val)
        );
      }
    });
  }
    
  registrationNavigate() : void{
    this.router.navigate(['/registration']);
  }
    
     

}

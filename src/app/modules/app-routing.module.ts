import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, RouterModule } from '@angular/router';
import { AutorizationComponent } from './autorization/autorization.component';
import { RegistrationComponent } from './registration/registration.component';
import { TableComponent } from './table/table.component';



@NgModule({
  declarations: [],
  imports: [
    CommonModule, RouterModule.forRoot([
      { path: 'autorization', loadChildren: () => import('./autorization/autorization.module').then(m=>m.AutorizationModule)}, 
      { path: 'registration', 
        loadChildren: () => import('./registration/registration.module').then(m=>m.RegistrationModule)},
      { path: 'tasks', component: TableComponent},
      { path: '', redirectTo:'autorization', pathMatch:'full'}
    ])
  ],
  exports:[RouterModule]
})
export class AppRoutingModule { }

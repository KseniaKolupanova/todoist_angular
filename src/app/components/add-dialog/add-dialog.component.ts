import { ChangeDetectionStrategy, Component, Inject, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject, takeUntil } from 'rxjs';
import { Priority, TaskStatus } from 'src/enum';
import { Task } from 'src/interface';
import { AutorizationService } from '../../services/autorization.service';




@Component({
  selector: 'app-add-dialog',
  templateUrl: './add-dialog.component.html',
  styleUrls: ['./add-dialog.component.scss'], 
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddDialogComponent implements OnDestroy{

  addForm : FormGroup;
  priority = Object.values(Priority);
  categoryAll:string[];
  minDate : Date = new Date();
  destroy$: Subject<boolean> = new Subject<boolean>();
  
  constructor(public autorizationService:AutorizationService, public dialogRef: MatDialogRef<AddDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: {name:string, category:string[]}) { 
    this.addForm = new FormGroup({
      nameTask: new FormControl(null, Validators.required),
      description: new FormControl(null),
      dateDeadline: new FormControl(null, [Validators.required]),
      priority:new FormControl(null, Validators.required),
      category:new FormControl(null)
    });
    this.categoryAll = data.category;
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  deleteCategory(option:string) : void{
    const index = this.categoryAll.indexOf(option);
    if (index !== -1) {
        this.categoryAll.splice(index, 1);
    }
    this.autorizationService.deleteCategory(option, this.autorizationService.currentUser$.value.userName)
    .pipe(takeUntil(this.destroy$))
    .subscribe();
    this.autorizationService.getUser()
    .pipe(takeUntil(this.destroy$))
    .subscribe(val=>
      this.autorizationService.currentUser$.next(val)
    );
  }

  addNewTask() : void{
    const formValue = this.addForm.getRawValue();
    const task:Task = {
      userName : this.data.name,
      nameTask : formValue.nameTask,
      description : formValue.description,
      status : TaskStatus.undone,
      priority : formValue.priority,
      category : formValue.category,
      dateDeadline : formValue.dateDeadline
    };
    this.dialogRef.close(task);   

  }

}

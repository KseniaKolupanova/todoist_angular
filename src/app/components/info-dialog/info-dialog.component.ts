import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BehaviorSubject } from 'rxjs';


@Component({
  selector: 'app-info-dialog',
  templateUrl: './info-dialog.component.html',
  styleUrls: ['./info-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InfoDialogComponent{

	header$ = new BehaviorSubject<string>("");
	description$ = new BehaviorSubject<string>("");

	constructor(public dialogRef: MatDialogRef<InfoDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: {header:string, description:string}){
		this.header$.next(data.header);
		this.description$.next(data.description);
		
	}

	public close() :void {
		this.dialogRef.close();
	}

}
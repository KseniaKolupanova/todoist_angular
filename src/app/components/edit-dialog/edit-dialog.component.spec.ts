import { CommonModule } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocomplete } from '@angular/material/autocomplete';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { AutorizationService } from 'src/app/services/autorization.service';
import { Priority, TaskStatus } from 'src/enum';
import { Task } from 'src/interface';
import { EditDialogComponent } from './edit-dialog.component';


const CATEGORIES = ["дом","работа","личное","хобби"];

const TASK : Task = {
    userName: "Ксения",
    nameTask : "Работать", 
    dateDeadline: new Date(),
    priority:Priority.one,
    category:"работа",
    description:"",
    status:TaskStatus.undone};

describe('EditDialogComponent', () => {
  let component: EditDialogComponent;
  let fixture: ComponentFixture<EditDialogComponent>;

  beforeEach(async () => {
    const name = "Ксения";
    
    
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        MatDialogModule,
        CommonModule,
        FormsModule, 
        ReactiveFormsModule,
        SharedModule,
        BrowserAnimationsModule 
      ],
      providers: [
        {
          provide: MatDialogRef,
          useValue: {}
        },
        { provide: MAT_DIALOG_DATA, useValue: {name:name, task:TASK, category:CATEGORIES} },
        AutorizationService
      ],
      declarations: [ 
        EditDialogComponent,
        MatAutocomplete 
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    
  });

  it('Проверить удаление категории из списка', () => {
    component.categoryAll = CATEGORIES;
    component.deleteCategory("дом");
    expect(component.categoryAll).not.toContain("дом");
  });

});

import { ChangeDetectionStrategy, Component, Inject, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject, takeUntil } from 'rxjs';
import { Priority, TaskStatus } from 'src/enum';
import { Task } from 'src/interface';
import { AutorizationService } from '../../services/autorization.service';

@Component({
  selector: 'app-edit-dialog',
  templateUrl: './edit-dialog.component.html',
  styleUrls: ['./edit-dialog.component.scss'], 
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditDialogComponent implements OnDestroy{

  changeForm : FormGroup;
  priority = Object.values(Priority);
  status = Object.values(TaskStatus);
  categoryAll:string[];
  minDate : Date = new Date();
  destroy$: Subject<boolean> = new Subject<boolean>();
  
  constructor(public autorizationService:AutorizationService, public dialogRef: MatDialogRef<EditDialogComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: {name:string, task:Task, category:string[]}) { 
    
    this.changeForm = new FormGroup({
      nameTask: new FormControl(data.task.nameTask, Validators.required),
      description: new FormControl(data.task.description),
      dateDeadline: new FormControl(data.task.dateDeadline, [Validators.required]),
      priority:new FormControl(data.task.priority, Validators.required),
      category:new FormControl(data.task.category),
      status:new FormControl(data.task.status, Validators.required)
    });
    this.categoryAll = data.category;
    
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  deleteCategory(option:string):void{
    
    const index = this.categoryAll.indexOf(option);
    if (index !== -1) {
        this.categoryAll.splice(index, 1);
    }
    this.autorizationService.deleteCategory(option,this.autorizationService.currentUser$.value.userName)
    .pipe(takeUntil(this.destroy$))
    .subscribe();
    this.autorizationService.getUser()
    .pipe(takeUntil(this.destroy$))
    .subscribe(val=>
      this.autorizationService.currentUser$.next(val)
    );
  }

  changeNewTask():void{    
    const formValue = this.changeForm.getRawValue();
    let task:Task = {
      userName : this.data.name,
      nameTask : formValue.nameTask,
      description : formValue.description,
      status : formValue.status,
      priority : formValue.priority,
      category : formValue.category,
      dateDeadline : formValue.dateDeadline
    };
    this.dialogRef.close(task);
  }

}

import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AddDialogComponent } from '../add-dialog/add-dialog.component';

@Component({
  selector: 'app-edit-deadline',
  templateUrl: './edit-deadline.component.html',
  styleUrls: ['./edit-deadline.component.scss'], 
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditDeadlineComponent {

  editForm : FormGroup;

  constructor(public dialogRef: MatDialogRef<AddDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: {day:number}) {
    this.editForm = new FormGroup({
      day: new FormControl(data.day, Validators.required)
    });
   }
  
  edit():void{
    this.dialogRef.close(this.editForm.get('day')?.value);
  }

}

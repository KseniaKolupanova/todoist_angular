import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { TableComponent } from '../../modules/table/table.component';
@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss'], 
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfirmComponent {

  constructor(public dialogRef: MatDialogRef<TableComponent>) {}

  confirmDelete():void{
    this.dialogRef.close(true);
  }

}

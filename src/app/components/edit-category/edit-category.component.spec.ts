import { CommonModule } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormArray, FormBuilder, FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RouterTestingModule } from '@angular/router/testing';
import { EditCategoryComponent } from './edit-category.component';

const CATEGORIES = ["дом","работа","личное","хобби"];

describe('EditCategoryComponent', () => {
  let component: EditCategoryComponent;
  let fixture: ComponentFixture<EditCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        MatDialogModule,
        CommonModule,
        FormsModule, 
        ReactiveFormsModule 
      ],
      providers: [
        {
          provide: MatDialogRef,
          useValue: {}
        },
        { provide: MAT_DIALOG_DATA, useValue: {} },
        FormBuilder
      ],
      declarations: [ EditCategoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Вернет true, если в категориях есть пустой элемент', ()=>{

    const categories: FormArray = new FormArray([...["дом","работа","личное","хобби",""].map(item => new FormControl(item))]
    );
    component.categoryForm.setControl('categories', categories); 
    const result = component.checkEmpty();
    expect(result).toBe(true);
  });

  it('Удалит категорию из массивов данных', () =>{
    const categories: FormArray = new FormArray([...CATEGORIES.map(item => new FormControl(item))]
    );
    component.categoryForm.setControl('categories', categories); 
    component.dataSource.data = CATEGORIES;
    component.deleteCategories("хобби");
    expect(component.categoryForm.get('categories')?.value).toEqual(["дом","работа","личное"]);
    expect(component.dataSource.data).toEqual(["дом","работа","личное"]);
  });

  it('Добавит пустую строку в массив данных', () =>{
    const categories: FormArray = new FormArray([...CATEGORIES.map(item => new FormControl(item))]
    );
    component.categoryForm.setControl('categories', categories); 
    component.dataSource.data = CATEGORIES;
    component.addCategory();
    expect(component.dataSource.data).toContain("");
  });
});

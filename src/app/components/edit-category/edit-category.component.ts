import { ChangeDetectionStrategy, Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTable, MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.scss'], 
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditCategoryComponent implements OnInit {

  displayedColumns: string[] = ['category', 'select'];
  dataSource = new MatTableDataSource<string>([]);
  flagInvalid :boolean;
  @ViewChild(MatTable) matTable : MatTable<Task>|undefined;
 
  categoryForm : FormGroup = this.fb.group({
    categories: this.fb.array([])
  });
  

  constructor(public dialogRef: MatDialogRef<EditCategoryComponent>, @Inject(MAT_DIALOG_DATA) public data: {category:string[]}, private fb : FormBuilder) {
    
    this.dataSource.data = data.category;
    this.flagInvalid = false;
  }

  private setCategoryForm():void{
    const categoryCtrl = this.categoryForm.get('categories') as FormArray;
    this.dataSource.data.forEach((cat)=>{
      categoryCtrl.push(this.setCategoriesFormArray(cat))
    });
  }

  private setCategoriesFormArray(cat:string):FormGroup{
    return this.fb.group({
        name:[cat]
    });
  }

  ngOnInit(): void {
    this.setCategoryForm();
  }

  deleteCategories(element:string ) : void{
    const index = this.dataSource.data.indexOf(element);
    this.dataSource.data = this.dataSource.data.filter(el => el!=element);
    const categoryCtrl = this.categoryForm.get('categories') as FormArray;
    categoryCtrl.removeAt(index);
    this.matTable?.renderRows();
  }


  addCategory() : void{
    this.dataSource.data.push("");
    const categoryCtrl = this.categoryForm.get('categories') as FormArray;
    categoryCtrl.push(this.setCategoriesFormArray(""));
    this.matTable?.renderRows();
  }

  save():void{

    if(!this.checkEmpty()){
      this.flagInvalid = false;      
      this.dialogRef.close(this.categoryForm.get('categories'));
    }
    else{
      this.flagInvalid = true;
    }
    
  }

  checkEmpty():boolean{
    const res = this.categoryForm.get('categories')?.value.reduce((prev:boolean,val:any)=> prev || val === "",false);
    return res;
  }


}

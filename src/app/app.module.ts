import { NgModule, Provider } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AutorizationService } from './services/autorization.service';
import { CheckPasswordService } from './services/check-password.service';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { AppRoutingModule } from './modules/app-routing.module';
import { SharedModule } from './modules/shared/shared.module';
import { TableModule } from './modules/table/table.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EditCategoryComponent } from './components/edit-category/edit-category.component';
import { EditDeadlineComponent } from './components/edit-deadline/edit-deadline.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpInterceptorService } from './services/http-interceptor.service';


const INTERCEPTOR_PROVIDER : Provider = {
  provide:HTTP_INTERCEPTORS,
  useClass:HttpInterceptorService,
  multi:true
}

@NgModule({
  declarations: [
    AppComponent,
    EditCategoryComponent,
    EditDeadlineComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedModule,
    TableModule,
    HttpClientModule
  ],
  providers: [
    AutorizationService, 
    CheckPasswordService, 
    { provide: MAT_DATE_LOCALE, useValue: 'ru-RU' },
    INTERCEPTOR_PROVIDER],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { BehaviorSubject, Subject, Subscription, takeUntil } from 'rxjs';
import { AutorizationService } from 'src/app/services/autorization.service';
import { User } from 'src/interface';
import { EditCategoryComponent } from './components/edit-category/edit-category.component';
import { EditDeadlineComponent } from './components/edit-deadline/edit-deadline.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection : ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit, OnDestroy{
  
  userInfo$ : BehaviorSubject<User> = new BehaviorSubject({} as User);
  subscription$ : Subscription = new Subscription();
  destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(private autorizationService: AutorizationService, private router:Router, public dialog: MatDialog) {}

  logOut() : void{
    this.autorizationService.logOutUser().subscribe();
    this.autorizationService.currentUser$.next({}as User)
    this.autorizationService.getUser()
    .pipe(takeUntil(this.destroy$))
    .subscribe(val => {this.userInfo$?.next(val)});
  }
  
  ngOnInit() : void{
    this.subscription$ = this.autorizationService.currentUser$.subscribe(val=>this.userInfo$.next(val)); 
    this.autorizationService.getUser()
    .pipe(takeUntil(this.destroy$))
    .subscribe(val => {this.userInfo$?.next(val)});
    //currentUser$.subscribe(val => {this.userInfo$?.next(val)});
    
    if(this.userInfo$?.value.userName === ""){
      this.router.navigate(['/autorization']);
    }
    else{
      this.autorizationService.currentUser$.next(this.userInfo$?.value);
      this.router.navigate(['/tasks']);
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  viewEditCategory():void{
    const dialogRef = this.dialog.open(EditCategoryComponent, {data : {category : this.userInfo$.value.categories}});
    dialogRef.afterClosed().subscribe(value => this.closeEditCategoryDialog.call(this,value));
  }

  closeEditCategoryDialog(value:FormArray) : void{
    if(value!==undefined)
     this.autorizationService.changeCategory(value, this.userInfo$.value.userName)
     .pipe(takeUntil(this.destroy$))
      .subscribe();
    this.autorizationService.getUser()
    .pipe(takeUntil(this.destroy$))
    .subscribe(val=>
      this.autorizationService.currentUser$.next(val)
    );
  }

  viewEditDeadline() : void{
    const dialogRef = this.dialog.open(EditDeadlineComponent, {data : {day : this.userInfo$.value.soonDeadline}});
    dialogRef.afterClosed().subscribe(value => this.closeEditDeadlineDialog.call(this,value));
  }

  closeEditDeadlineDialog(value:number) : void{
    
    if(value){
      this.autorizationService.changeDeadline({userName:this.userInfo$.value.userName, deadLine:value})
      .pipe(takeUntil(this.destroy$))
      .subscribe();
      const user : User = this.userInfo$.value;
      user.soonDeadline = value;
      this.autorizationService.currentUser$.next(user);
    } 
     
  }
}
